# Telegraf Configuration
#
# Telegraf is entirely plugin driven. All metrics are gathered from the
# declared inputs, and sent to the declared outputs.
#
# Plugins must be declared in here to be active.
# To deactivate a plugin, comment out the name and any variables.
#
# Use 'telegraf -config telegraf.conf -test' to see what metrics a config
# file would generate.
#
# Environment variables can be used anywhere in this config file, simply prepend
# them with $. For strings the variable must be within quotes (ie, "$STR_VAR"),
# for numbers and booleans they should be plain (ie, $INT_VAR, $BOOL_VAR)


# Global tags can be specified here in key="value" format.
[global_tags]
  # dc = "us-east-1" # will tag all metrics with dc=us-east-1
  # rack = "1a"
  ## Environment variables can be used as tags, and throughout the config file
  # user = "$USER"


# Configuration for telegraf agent
[agent]
  ## Default data collection interval for all inputs
  interval = "10s"
  ## Rounds collection interval to 'interval'
  ## ie, if interval="10s" then always collect on :00, :10, :20, etc.
  round_interval = true

  ## Telegraf will send metrics to outputs in batches of at most
  ## metric_batch_size metrics.
  ## This controls the size of writes that Telegraf sends to output plugins.
  metric_batch_size = 1000

  ## For failed writes, telegraf will cache metric_buffer_limit metrics for each
  ## output, and will flush this buffer on a successful write. Oldest metrics
  ## are dropped first when this buffer fills.
  ## This buffer only fills when writes fail to output plugin(s).
  metric_buffer_limit = 10000

  ## Collection jitter is used to jitter the collection by a random amount.
  ## Each plugin will sleep for a random time within jitter before collecting.
  ## This can be used to avoid many plugins querying things like sysfs at the
  ## same time, which can have a measurable effect on the system.
  collection_jitter = "0s"

  ## Default flushing interval for all outputs. Maximum flush_interval will be
  ## flush_interval + flush_jitter
  flush_interval = "10s"
  ## Jitter the flush interval by a random amount. This is primarily to avoid
  ## large write spikes for users running a large number of telegraf instances.
  ## ie, a jitter of 5s and interval 10s means flushes will happen every 10-15s
  flush_jitter = "0s"

  ## By default or when set to "0s", precision will be set to the same
  ## timestamp order as the collection interval, with the maximum being 1s.
  ##   ie, when interval = "10s", precision will be "1s"
  ##       when interval = "250ms", precision will be "1ms"
  ## Precision will NOT be used for service inputs. It is up to each individual
  ## service input to set the timestamp at the appropriate precision.
  ## Valid time units are "ns", "us" (or "µs"), "ms", "s".
  precision = ""

  ## Logging configuration:
  ## Run telegraf with debug log messages.
  debug = false
  ## Run telegraf in quiet mode (error log messages only).
  quiet = false
  ## Specify the log file name. The empty string means to log to stderr.
  logfile = ""

  ## Override default hostname, if empty use os.Hostname()
  hostname = ""
  ## If set to true, do no set the "host" tag in the telegraf agent.
  omit_hostname = false

###############################################################################
#                            OUTPUT PLUGINS                                   #
###############################################################################

# Configuration for sending metrics to InfluxDB
[[outputs.influxdb]]
  ## The full HTTP or UDP URL for your InfluxDB instance.
  ##
  ## Multiple URLs can be specified for a single cluster, only ONE of the
  ## urls will be written to each interval.
  # urls = ["unix:///var/run/influxdb.sock"]
  # urls = ["udp://127.0.0.1:8089"]
  # urls = ["http://127.0.0.1:8086"]
    urls = ["http://influxdb:8086"]

  ## The target database for metrics; will be created as needed.
  ## For UDP url endpoint database needs to be configured on server side.
    database = "nas_telegraf"

  ## The value of this tag will be used to determine the database.  If this
  ## tag is not set the 'database' option is used as the default.
  # database_tag = ""

  ## If true, no CREATE DATABASE queries will be sent.  Set to true when using
  ## Telegraf with a user without permissions to create databases or when the
  ## database already exists.
    skip_database_creation = true

  ## Name of existing retention policy to write to.  Empty string writes to
  ## the default retention policy.  Only takes effect when using HTTP.
    retention_policy = ""

  ## Write consistency (clusters only), can be: "any", "one", "quorum", "all".
  ## Only takes effect when using HTTP.
  # write_consistency = "any"

  ## Timeout for HTTP messages.
    timeout = "5s"

  ## HTTP Basic Auth
    username = "nas_telegraf"
    password = "nas_telegraf"

  ## HTTP User-Agent
  # user_agent = "telegraf"

  ## UDP payload size is the maximum packet size to send.
  # udp_payload = "512B"

  ## Optional TLS Config for use on HTTP connections.
  # tls_ca = "/etc/telegraf/ca.pem"
  # tls_cert = "/etc/telegraf/cert.pem"
  # tls_key = "/etc/telegraf/key.pem"
  ## Use TLS but skip chain & host verification
  # insecure_skip_verify = false

  ## HTTP Proxy override, if unset values the standard proxy environment
  ## variables are consulted to determine which proxy, if any, should be used.
  # http_proxy = "http://corporate.proxy:3128"

  ## Additional HTTP headers
  # http_headers = {"X-Special-Header" = "Special-Value"}

  ## HTTP Content-Encoding for write request body, can be set to "gzip" to
  ## compress body or "identity" to apply no encoding.
  # content_encoding = "identity"

  ## When true, Telegraf will output unsigned integers as unsigned values,
  ## i.e.: "42u".  You will need a version of InfluxDB supporting unsigned
  ## integer values.  Enabling this option will result in field type errors if
  ## existing data has been written.
  # influx_uint_support = false

###############################################################################
#                            INPUT PLUGINS                                    #
###############################################################################
##
## Retrieves details via SNMP from remote agents
##

##
## Synology 
##
 [[inputs.snmp]]
   # List of agents to poll
   agents = [  "192.168.1.2", "192.168.1.1" ]
   # Polling interval
   interval = "60s"
   # Timeout for each SNMP query.
   timeout = "10s"
   # Number of retries to attempt within timeout.
   retries = 3
   # SNMP version, UAP only supports v1
   version = 2
   # SNMP community string.
   community = "public"
   # The GETBULK max-repetitions parameter
   max_repetitions = 30
   # Measurement name
   name = "snmp.SYNO"


   ##
   ## Synology Ports
   ##
   #  System Port
   [[inputs.snmp.field]]
     name = "ethPortStatus"
     oid = "SYNOLOGY-PORT-MIB::ethPortStatus.2"
   #  System Port
   [[inputs.snmp.field]]
     name = "ethPortSpeed"
     oid = "SYNOLOGY-PORT-MIB::ethPortSpeed.3"
     
   ##
   ## System Details
   ##
   #  System name (hostname)
   [[inputs.snmp.field]]
     is_tag = true
     name = "sysName"
     oid = "RFC1213-MIB::sysName.0"
   #  System vendor OID
   [[inputs.snmp.field]]
     name = "sysObjectID"
     oid = "RFC1213-MIB::sysObjectID.0"
   #  System description
   [[inputs.snmp.field]]
     name = "sysDescr"
     oid = "RFC1213-MIB::sysDescr.0"
   #  System contact
   [[inputs.snmp.field]]
     name = "sysContact"
     oid = "RFC1213-MIB::sysContact.0"
   #  System location
   [[inputs.snmp.field]]
     name = "sysLocation"
     oid = "RFC1213-MIB::sysLocation.0"
   #  System uptime
   [[inputs.snmp.field]]
     name = "sysUpTime"
     oid = "RFC1213-MIB::sysUpTime.0"
   # Inet interface
   [[inputs.snmp.table]]
     oid = "IF-MIB::ifTable"
     [[inputs.snmp.table.field]]
       is_tag = true
     oid = "IF-MIB::ifDescr"
   #Syno disk
   [[inputs.snmp.table]]
     oid = "SYNOLOGY-DISK-MIB::diskTable"
     [[inputs.snmp.table.field]]
       is_tag = true
     oid = "SYNOLOGY-DISK-MIB::diskID" 
   #Syno raid
   [[inputs.snmp.table]]
     oid = "SYNOLOGY-RAID-MIB::raidTable"
     [[inputs.snmp.table.field]]
       is_tag = true
     oid = "SYNOLOGY-RAID-MIB::raidName" 
   #Syno load
   [[inputs.snmp.table]]
     oid = "UCD-SNMP-MIB::laTable"
     [[inputs.snmp.table.field]]
       is_tag = true
     oid = "UCD-SNMP-MIB::laNames"
   #  System memTotalSwap
   [[inputs.snmp.field]]
     name = "memTotalSwap"
     oid = "UCD-SNMP-MIB::memTotalSwap.0"
   #  System memAvailSwap
   [[inputs.snmp.field]]
     name = "memAvailSwap"
     oid = "UCD-SNMP-MIB::memAvailSwap.0"
   #  System memTotalReal
   [[inputs.snmp.field]]
     name = "memTotalReal"
     oid = "UCD-SNMP-MIB::memTotalReal.0"
   #  System memAvailReal
   [[inputs.snmp.field]]
     name = "memAvailReal"
     oid = "UCD-SNMP-MIB::memAvailReal.0"
   #  System memTotalFree
   [[inputs.snmp.field]]
     name = "memTotalFree"
     oid = "UCD-SNMP-MIB::memTotalFree.0"
   #  System Status
   [[inputs.snmp.field]]
     name = "systemStatus"
     oid = "SYNOLOGY-SYSTEM-MIB::systemStatus.0"
   #  System temperature
   [[inputs.snmp.field]]
     name = "temperature"
     oid = "SYNOLOGY-SYSTEM-MIB::temperature.0"
   #  System powerStatus
   [[inputs.snmp.field]]
     name = "powerStatus"
     oid = "SYNOLOGY-SYSTEM-MIB::powerStatus.0"
   #  System systemFanStatus
   [[inputs.snmp.field]]
     name = "systemFanStatus"
     oid = "SYNOLOGY-SYSTEM-MIB::systemFanStatus.0"
   #  System cpuFanStatus
   [[inputs.snmp.field]]
     name = "cpuFanStatus"
     oid = "SYNOLOGY-SYSTEM-MIB::cpuFanStatus.0"
   #  System modelName
   [[inputs.snmp.field]]
     name = "modelName"
     oid = "SYNOLOGY-SYSTEM-MIB::modelName.0"
   #  System serialNumber
   [[inputs.snmp.field]]
     name = "serialNumber"
     oid = "SYNOLOGY-SYSTEM-MIB::serialNumber.0"
   #  System version
   [[inputs.snmp.field]]
     name = "version"
     oid = "SYNOLOGY-SYSTEM-MIB::version.0"
   #  System upgradeAvailable
   [[inputs.snmp.field]]
     name = "upgradeAvailable"
     oid = "SYNOLOGY-SYSTEM-MIB::upgradeAvailable.0"
   # System volume   
   [[inputs.snmp.table]]
     oid = "HOST-RESOURCES-MIB::hrStorageTable"
   [[inputs.snmp.table.field]]
       is_tag = true
     oid = "HOST-RESOURCES-MIB::hrStorageDescr"
   # System ssCpuUser 
   [[inputs.snmp.field]]
     name = "ssCpuUser"
     oid = ".1.3.6.1.4.1.2021.11.9.0"

   # System memBuffer
   [[inputs.snmp.field]]
     name = "memBuffer"
     oid = ".1.3.6.1.4.1.2021.4.14.0"

   # System memCached 
   [[inputs.snmp.field]]
     name = "memCached"
     oid = ".1.3.6.1.4.1.2021.4.15.0"

   # System ssCpuSystem  
   [[inputs.snmp.field]]
     name = "ssCpuSystem"
     oid = ".1.3.6.1.4.1.2021.11.10.0"
   # System ssCpuIdle   
   [[inputs.snmp.field]]
     name = "ssCpuIdle"
     oid = ".1.3.6.1.4.1.2021.11.11.0"
   # Service users CIFS
   [[inputs.snmp.table.field]]
     name = "usersCIFS"
     oid = "SYNOLOGY-SERVICES-MIB::serviceUsers"
     oid_index_suffix = "1"
   # Service users AFP
   [[inputs.snmp.table.field]]
     name = "usersAFP"
     oid = "SYNOLOGY-SERVICES-MIB::serviceUsers"
     oid_index_suffix = "2"
   # Service users NFS
   [[inputs.snmp.table.field]]
     name = "usersNFS"
     oid = "SYNOLOGY-SERVICES-MIB::serviceUsers"
     oid_index_suffix = "3"
   # Service users FTP
   [[inputs.snmp.table.field]]
     name = "usersFTP"
     oid = "SYNOLOGY-SERVICES-MIB::serviceUsers"
     oid_index_suffix = "4"
   # Service users SFTP
   [[inputs.snmp.table.field]]
     name = "usersSFTP"
     oid = "SYNOLOGY-SERVICES-MIB::serviceUsers"
     oid_index_suffix = "5"
   # Service users HTTP
   [[inputs.snmp.table.field]]
     name = "usersHTTP"
     oid = "SYNOLOGY-SERVICES-MIB::serviceUsers"
     oid_index_suffix = "6"
   # Service users TELNET
   [[inputs.snmp.table.field]]
     name = "usersTELNET"
     oid = "SYNOLOGY-SERVICES-MIB::serviceUsers"
     oid_index_suffix = "7"
   # Service users SSH
   [[inputs.snmp.table.field]]
     name = "usersSSH"
     oid = "SYNOLOGY-SERVICES-MIB::serviceUsers"
     oid_index_suffix = "8"
   # Service users OTHER
   [[inputs.snmp.table.field]]
     name = "usersOTHER"
     oid = "SYNOLOGY-SERVICES-MIB::serviceUsers"
     oid_index_suffix = "9"
   # UPS Status
   [[inputs.snmp.table.field]]
     name = "upsStatus"
     oid = "SYNOLOGY-UPS-MIB::upsInfoStatus"
   # UPS Load
   [[inputs.snmp.table.field]]
     name = "upsLoad"
     oid = "SYNOLOGY-UPS-MIB::upsInfoLoadValue"
   # UPS Battery Charge
   [[inputs.snmp.table.field]]
     name = "upsCharge"
     oid = "SYNOLOGY-UPS-MIB::upsBatteryChargeValue"
   # UPS Battery Charge Warning
   [[inputs.snmp.table.field]]
     name = "upsWarning"
     oid = "SYNOLOGY-UPS-MIB::upsBatteryChargeWarning"


# # Read metrics about docker containers
  [[inputs.docker]]
#   ## Docker Endpoint
#   ##   To use TCP, set endpoint = "tcp://[ip]:[port]"
#   ##   To use environment variables (ie, docker-machine), set endpoint = "ENV"
    endpoint = "unix:///var/run/docker.sock"
#
#   ## Set to true to collect Swarm metrics(desired_replicas, running_replicas)
#   gather_services = false
#
#   ## Only collect metrics for these containers, collect all if empty
    container_names = []
#
#   ## Set the source tag for the metrics to the container ID hostname, eg first 12 chars
#   source_tag = false
#
#   ## Containers to include and exclude. Globs accepted.
#   ## Note that an empty array for both will include all containers
#   container_name_include = []
#   container_name_exclude = []
#
#   ## Container states to include and exclude. Globs accepted.
#   ## When empty only containers in the "running" state will be captured.
#   ## example: container_state_include = ["created", "restarting", "running", "removing", "paused", "exited", "dead"]
#   ## example: container_state_exclude = ["created", "restarting", "running", "removing", "paused", "exited", "dead"]
    container_state_include = ["created", "restarting", "running", "removing", "paused", "exited", "dead"]
    container_state_exclude = []
#
#   ## Timeout for docker list, info, and stats commands
    timeout = "5s"
#
#   ## Whether to report for each container per-device blkio (8:0, 8:1...) and
#   ## network (eth0, eth1, ...) stats or not
    perdevice = true
#
#   ## Whether to report for each container total blkio and network stats or not
    total = false
#
#   ## Which environment variables should we use as a tag
#   ##tag_env = ["JAVA_HOME", "HEAP_SIZE"]
#
#   ## docker labels to include and exclude as tags.  Globs accepted.
#   ## Note that an empty array for both will include all labels as tags
    docker_label_include = []
    docker_label_exclude = []
#
#   ## Optional TLS Config
#   # tls_ca = "/etc/telegraf/ca.pem"
#   # tls_cert = "/etc/telegraf/cert.pem"
#   # tls_key = "/etc/telegraf/key.pem"
#   ## Use TLS but skip chain & host verification
#   # insecure_skip_verify = false
